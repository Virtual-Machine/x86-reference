# x86-reference

My attempt to break down x86 so I can better understand the architecture.

## To Do

[ ] - Determine road map.

## Useful References

[Intel x86 Manuals](https://software.intel.com/content/www/us/en/develop/articles/intel-sdm.html#combined)

## Visual References

### 32 bit environment

![32bit Registers](reg32.png)

### 64 bit environment

![64bit Registers](reg64.png)

### General purpose registers

![General Purpose Registers](general_regs.png)
![General Purpose Registers 2](general_regs2.png)

### Memory management models

![Memory Management](memory_models.png)
![Segment Registers](segment_registers.png)

### Eflags

![Eflags](eflags.png)

### Stack

![Stack](stack.png)

### Overview

![32bit](system_overview.png)
![64bit](system_overview64.png)
